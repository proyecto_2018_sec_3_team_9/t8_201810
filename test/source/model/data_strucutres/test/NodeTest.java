package model.data_strucutres.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Node;
import model.vo.Taxi;

public class NodeTest
{
	/**
	 *A new node to be used in the test.
	 */
	private Node<Taxi> node;

	/**
	 *A new node to be used in the test.
	 */
	private Node<Taxi> node1;

	/**
	 *A new node to be used in the test.
	 */
	private Node<Taxi> node2;

	/**
	 * a new service element to be in the node.
	 */
	private Taxi taxi;

	/**
	 * a new service element to be in the node.
	 */
	private Taxi taxi1;

	/**
	 * a new service element to be in the node.
	 */
	private Taxi taxi2;
	/**
	 * Creates a new Service and a node with it. This method should be used in every test.
	 */
	@Before
	public void setupEscenary( )
	{
		taxi = new Taxi();
		taxi.setTaxiId("123");
		taxi.setCompany("Compañia del sabor");
		node = new Node<Taxi>(taxi);
	}

	/**
	 * Creates a new Service and a node with it. This method should be used in every test.
	 */
	@Before
	public void setupEscenary2( )
	{
		taxi = new Taxi();
		taxi.setTaxiId("123");
		taxi.setCompany("Compañia del sabor");
		node = new Node<Taxi>(taxi);
		node.changeNext(node1);
		taxi1 = new Taxi();
		taxi1.setTaxiId("1234");
		taxi1.setCompany("Compañia tappsi");
		node1 = new Node<Taxi>(taxi1);
		taxi2 = new Taxi();
		taxi2.setTaxiId("12345");
		taxi2.setCompany("Compañia cabify");
		node2 = new Node<Taxi>(taxi2);
	}

	/**
	 * Test 1: Verifies method getElement(). <br>
	 * <b>Methds to prove:</b> <br>
	 * getElement
	 */
	@Test
	public void testGetElement( )
	{

		setupEscenary();
		// Case1
		assertEquals( "The element doesn´t match.", taxi, node.getElement());

	}

	/**
	 * Test 2: Verifies method getNext(). <br>
	 * <b>Methds to prove:</b> <br>
	 * getNext
	 * <b> Cases: </b><br>
	 * 1) No next node. <br>
	 * 2) There is a next node.
	 */
	@Test
	public void testGetNext( )
	{
		setupEscenary();
		// Case1
		assertNull( "The next element should be null.", node.getNext());
		//Case2
		setupEscenary2();
		assertEquals("The next element should be node 1", node1, node.getNext());
	}

	/**
	 * Test 3: Verifies method changeNext(). <br>
	 * <b>Methds to prove:</b> <br>
	 * Node<br>
	 * getNext<br>
	 * changeNext<br>
	 * <b> Cases: </b><br>
	 * 1) There is no next node. <br>
	 * 2) There is a next node.
	 */
	@Test
	public void testChangeNext( )
	{
		//Case1
		setupEscenary();
		node.changeNext(node1);
		assertNotNull("The next node isn't supposed to be null anymore", node.getNext());
		assertEquals("The next node should be node 1", node1, node.getNext());
		//Case2
		setupEscenary2();
		node.changeNext(node2);
		assertNotEquals("The next node shouldn't be node1 anymore", node1, node.getNext());
		assertEquals("The next node now should be node2", node2, node.getNext());
	}


}
