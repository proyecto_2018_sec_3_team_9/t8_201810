package controller;

import java.text.ParseException;


import api.ITaxiTripsManager;
import model.logic.TaxiTripsManager;
import model.vo.Service;
import model.vo.Taxi;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean loadServices(String direccionJson, double distanciaReferencia, boolean usarPersistencia)
	{
		return manager.loadServices(direccionJson, distanciaReferencia, usarPersistencia);
	}
	
	public static String generarJSON()
	{
		return manager.generarJSON();
	}
}

