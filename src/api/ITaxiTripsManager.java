package api;

import model.vo.Taxi;


import java.text.ParseException;
import model.vo.Service;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load all the services in a graph using their coordinates and a reference distance.
	 * The services are loaded in a DiGraph
	 * @param servicesFile - path to the JSON file with taxi services
	 * @param distanciaRef - The reference distance to use
	 * @param usarPersistencia - Indicates if the data should be loaded from a previously created file or not.
	 * @return true if the services are correctly loaded, false if not. 
	 */
	public boolean loadServices(String serviceFile, double distanciaRef, boolean usarPersistencia);
	
	public String generarJSON();
}
