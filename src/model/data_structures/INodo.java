package model.data_structures;

public interface INodo<T> 

{
   
	public T darElemento( );
	
	public NodoLista<T> darSiguiente( );
	
    public NodoLista<T> darAnterior( );
    
    public void insertarAntes( NodoLista<T> nodo );
    
    public void insertarDespues( NodoLista<T> nodo );
    
    public NodoLista<T> desconectarPrimero( );
    
    public void desconectarNodo( );
    
    public String toString( );
   
}
