package model.data_structures;

import java.io.Serializable;

/**
 * Clase auxiliar del heap para poder instanciar un arreglo de objetos comparables. Representa un elemento de la secuencia.
 * @param <T> El tipo de elementos manejados en la secuencia
 */
public class Elemento<T extends Comparable<? super T>> extends Object implements Comparable<Elemento<T>>, Serializable
{
    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------
	
    /**
	 * Constante para la serializaci�n
	 */
	private static final long serialVersionUID = 1L;	

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	
    /**
     * El elemento
     */
    private T elemento;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------
    
    /**
     * Constructor del elemento. <br>
     * <b> post: </b>Se construy� un objeto de tipo Elemento con la informaci�n especificada
     * @param elem El elemento que contendr� el objeto
     */
    public Elemento( T elem )
    {
        elemento = elem;
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------
    
    /**
     * Retorna el elemento que contiene el objeto. <br>
     * <b> post: </b> Se retorn� el elemento que contiene el objeto.
     * @return Se retorn� el elemento que contiene el objeto
     */
    public T darElemento( )
    {
        return elemento;
    }

    /**
     * Compara el elemento con el objeto especificado. <br>
     * <b> post: </b> Se retorn� 0 si los objetos son iguales, 1 si el objeto actual es mayor al especificado o -1 si es menor.
     * @param elem Elemento con el que se va a realizar la comparaci�n
     * @return 0 si los objetos son iguales, 1 si el objeto actual es mayor al especificado o -1 si es menor
     */
    public int compareTo( Elemento<T> elem )
    {
        return elemento.compareTo( elem.elemento );
    }
}
