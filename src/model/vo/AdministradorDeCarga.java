package model.vo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;

import api.ITaxiTripsManager;
import model.data_structures.ArcoNoExisteException;
import model.data_structures.ArcoYaExisteException;
import model.data_structures.GrafoDirigido;
import model.data_structures.VerticeNoExisteException;
import model.data_structures.VerticeYaExisteException;

public class AdministradorDeCarga {
	private double distanciaReferencia;
    private GrafoDirigido<String, VerticeLatLongServicios, ArcoDistanciaTiempoValor> grafo;

	public AdministradorDeCarga(double distanciaReferencia)
	{
		this.distanciaReferencia=distanciaReferencia;
		grafo=new GrafoDirigido<String, VerticeLatLongServicios, ArcoDistanciaTiempoValor>();
	}

	public void agregarServicio(String tripId, double seconds, double miles, double total,double pickUpLatitude,double pickUpLongitude,double dropOffLatitude,double dropOffLongitude, double peajes) throws VerticeNoExisteException, ArcoYaExisteException  
	{
		//si no tenemos la informacion de alguna de estas no tendriamos info suficiente para agregar los nodos
		try
		{
			VerticeLatLongServicios inicio=agregarModificarVertice(tripId,pickUpLatitude,pickUpLongitude);
			VerticeLatLongServicios fin=agregarModificarVertice(tripId,dropOffLatitude,dropOffLongitude);
			try
			{
				ArcoDistanciaTiempoValor arco = (ArcoDistanciaTiempoValor) grafo.darArco(inicio.darId(), fin.darId());
				arco.agregarValores(miles*1609.34, seconds, total, peajes);
			}
			catch (ArcoNoExisteException e)
			{
				grafo.agregarArco(inicio.darId(), fin.darId(),new ArcoDistanciaTiempoValor(inicio.darId(), fin.darId(),miles*1609.34, seconds, total, peajes));
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		
	}
	
	public void agregarArco(ArcoDistanciaTiempoValor arco)
	{
		
			try 
			{
				grafo.agregarArco(arco.getIdVerticeOrigen(), arco.getIdVerticeDestino(),arco);
			} catch (VerticeNoExisteException | ArcoYaExisteException e) 
			{
				e.printStackTrace();
			}
		
	}
	public void agregarVertice(VerticeLatLongServicios vertice)
	{
		try
		{
			grafo.agregarVertice(vertice);
		} catch (VerticeYaExisteException e)
		{
			e.printStackTrace();
		}
	}
	
	public VerticeLatLongServicios agregarModificarVertice(String tripId,double latitude,double longitude) throws VerticeYaExisteException
	{
		VerticeLatLongServicios vertice=null;
		double distanciaMin=Double.POSITIVE_INFINITY;
		for(VerticeLatLongServicios actual:grafo.darVertices())
		{
			double dist=distanciaEnMetros(actual.darLatitud(),actual.darLongitud(),latitude,longitude);
			if(dist<distanciaMin)
				vertice=actual;
		}
		if(vertice==null)
		{
			vertice=new VerticeLatLongServicios(latitude,longitude,tripId);
			grafo.agregarVertice(vertice);
			return vertice;
		}
		vertice.agregarServicio(tripId);
		return vertice;

	}

	public double distanciaEnMetros(double pickupLatitud, double pickupLongitud,double pLat, double pLon) 
	{
		try
		{
			int r=6371*1000;
			Double latDist=toRad(pickupLatitud-pLat);
			Double lonDist=toRad(pickupLongitud-pLon);
			Double a = Math.sin(latDist/2)*Math.sin(latDist/2)
					+Math.cos(toRad(pLat))*Math.cos(toRad(pickupLatitud))
					*Math.sin(lonDist/2)*Math.sin(lonDist/2);
			Double c=2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			return distanciaReferencia>=(r*c)?(r*c):Double.POSITIVE_INFINITY;
			 

		}
		catch(Exception e)
		{ 	
			e.printStackTrace();
			System.out.println("entro");
			return Double.POSITIVE_INFINITY;
		}
	}
	public double toRad(double grados)
	{
		return (grados/90)*3.1416;
	}
	
	public void imprimirInfo()
	{
		System.out.println("Informaci�n del grafo: ");
		System.out.println("--N�mero de v�rtices: "+grafo.darVertices().size());
		System.out.println("--N�mero de arcos: "+grafo.darArcos().size());
		System.out.println("--Distancia utilizada: "+distanciaReferencia+" m�tros");

		
	}
	
	public String generarJSON(String tamanoDatos, String distanciaDeReferencia)
	{
		File carpetaCompleta = new File("./data/grafos/grafo-"+tamanoDatos+"-"+distanciaReferencia+"m");
		File archivoArcos = new File("./data/grafos/grafo-"+tamanoDatos+"-"+distanciaReferencia+"m/arcos-"+tamanoDatos+"-"+distanciaReferencia+"m.json");
		File archivoVertices = new File("./data/grafos/grafo-"+tamanoDatos+"-"+distanciaReferencia+"m/vertices-"+tamanoDatos+"-"+distanciaReferencia+"m.json");
		Gson gson=new Gson();
		
	
		try 
		{
			if(!carpetaCompleta.exists()) {
				carpetaCompleta.mkdir();
			}
			if(!archivoVertices.exists())
			{
				archivoVertices.createNewFile();
		
				PrintWriter pw = new PrintWriter(archivoVertices);
				pw.println(gson.toJson(grafo.darVertices()));
				pw.close();
				
			
				if(!archivoArcos.exists())
					archivoArcos.createNewFile();
			
				PrintWriter pw1 = new PrintWriter(archivoArcos);
				pw1.println(gson.toJson(grafo.darArcos()));
				pw1.close();
				return "El JSON se genero correctamente";
			}
			return "El JSON ya había sido generado";
			
		}
		catch (FileNotFoundException e) {
			return "Hubo un problema escribiendo el JSON";
		} catch (IOException e) {
			return "Hubo un problema escribiendo el JSON";
		}
	}

}
