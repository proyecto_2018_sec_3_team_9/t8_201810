package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;


import api.ITaxiTripsManager;
import model.vo.AdministradorDeCarga;
import model.vo.ArcoDistanciaTiempoValor;
import model.vo.Service;
import model.vo.VerticeLatLongServicios;


public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";

	private AdministradorDeCarga admin;
	private String tamanho;
	private double distanciaRef;

	@Override 
	public boolean loadServices(String direccionJson, double distanciaRef, boolean seUsaPersistencia)
	{

		this.distanciaRef = distanciaRef;

		try {
			admin = new AdministradorDeCarga(distanciaRef);
			if(seUsaPersistencia) {
				if(cargarVertices()&&cargarArcos())
					admin.imprimirInfo();
				else return false;
			}
			else {
				cargarArchivo(direccionJson);
			}

		}
		catch(Exception e) {
			e.printStackTrace();
		}	
		return false;
	}

	public void cargarArchivo(String direccionJson) {

		File arch = new File(direccionJson);
		int contador = 0;
		int contadorAlt = 0;

		if(arch.isDirectory()) {
			tamanho = "LARGE";
			File[] files = arch.listFiles();
			for(int i = 0; i<files.length; i++) {
				try {
					InputStream inputStream = new FileInputStream(files[i].getAbsolutePath());

					JsonReader lector = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
					Gson gson = new GsonBuilder().create();

					lector.beginArray();


					while(lector.hasNext()) {
						Service service = gson.fromJson(lector, Service.class);
						if(service.getPickup_centroid_latitude()!=null&&service.getPickup_centroid_longitude()!=null&&service.getDropoff_centroid_latitude()!=null&&service.getDropoff_centroid_longitude()!=null)
						{
							admin.agregarServicio(service.getTrip_id()==null?"":service.getTrip_id(),
									service.getTrip_seconds()==null?0:Double.parseDouble(service.getTrip_seconds()),
									service.getTrip_miles()==null?0:Double.parseDouble(service.getTrip_miles()),
									service.getTrip_total()==null? 0:Double.parseDouble(service.getTrip_total()),
									Double.parseDouble(service.getPickup_centroid_latitude()),
									Double.parseDouble(service.getPickup_centroid_longitude()),
									Double.parseDouble(service.getDropoff_centroid_latitude()),
									Double.parseDouble(service.getDropoff_centroid_longitude()),
									service.getTolls()==null? 0:Double.parseDouble(service.getTolls()));
							
							contadorAlt++;
						}
						contador++;
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}

			}
		}
		else {
			if(direccionJson.equals(DIRECCION_SMALL_JSON)) {
				tamanho = "SMALL";
			}
			else {
				tamanho = "MEDIUM";
			}
			try {
				InputStream inputStream = new FileInputStream(direccionJson);

				JsonReader lector = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
				Gson gson = new GsonBuilder().create();

				lector.beginArray();


				while(lector.hasNext()) {
					Service service = gson.fromJson(lector, Service.class);
					if(service.getPickup_centroid_latitude()!=null&&service.getPickup_centroid_longitude()!=null&&service.getDropoff_centroid_latitude()!=null&&service.getDropoff_centroid_longitude()!=null)
					{
						admin.agregarServicio(service.getTrip_id()==null?"":service.getTrip_id(),
								service.getTrip_seconds()==null?0:Double.parseDouble(service.getTrip_seconds()),
								service.getTrip_miles()==null?0:Double.parseDouble(service.getTrip_miles()),
								service.getTrip_total()==null? 0:Double.parseDouble(service.getTrip_total()),
								Double.parseDouble(service.getPickup_centroid_latitude()),
								Double.parseDouble(service.getPickup_centroid_longitude()),
								Double.parseDouble(service.getDropoff_centroid_latitude()),
								Double.parseDouble(service.getDropoff_centroid_longitude()),
								service.getTolls()==null? 0:Double.parseDouble(service.getTolls()));
						contadorAlt++;
					}
					contador++;
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("El tamaño TOTAL de la lista de servicios es:" + contador);
		System.out.println("La cantidad de servicios válidos (con latitudes y longitudes iniciales y finales) agregados al grafo es:" +contadorAlt);
		admin.imprimirInfo();
	}


	public boolean cargarArcos() throws Exception 
	{
		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json

		InputStream inputStream;
		try 
		{
			inputStream = new FileInputStream("./data/grafos/grafo-"+tamanho+"-"+distanciaRef+"m/arcos-"+tamanho+"-"+distanciaRef+"m.json");
		} catch (FileNotFoundException e1) {
			System.out.println("La persistencia de los arcos que desea cargar no existe.");
			return false;
		}

		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();

		// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
		reader.beginArray();
		try
		{
			while (reader.hasNext())
			{

				// Instancia un servicio del json
				ArcoDistanciaTiempoValor nArco = gson.fromJson(reader, ArcoDistanciaTiempoValor.class);

				admin.agregarArco(nArco);

			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		reader.close();
		return true;
	}

	public boolean cargarVertices() throws Exception
	{
		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json
		InputStream inputStream;
		try
		{
			inputStream = new FileInputStream("./data/grafos/grafo-"+tamanho+"-"+distanciaRef+"m/vertices-"+tamanho+"-"+distanciaRef+"m.json");
		} 
		catch (FileNotFoundException e1) 	
		{
			System.out.println("La persistencia de los vertices que desea cargar no existe.");
			return false;
		}
		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();

		// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
		reader.beginArray();
		try
		{
			while (reader.hasNext())
			{
				// Instancia un servicio del json
				VerticeLatLongServicios nVertice = gson.fromJson(reader, VerticeLatLongServicios.class);

				admin.agregarVertice(nVertice);

			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		reader.close();
		return true;
	}

	public String generarJSON()
	{
		return 	admin.generarJSON(tamanho,distanciaRef+"");

	}
}