package view;


import java.util.Scanner;


import controller.Controller;

import model.logic.TaxiTripsManager;


/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			
			case 1: // cargar informacion a procesar

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}


				printMenuDistancia();
				int distanciaRef = sc.nextInt();
				int distanciaReferencia = 0;
				switch (distanciaRef)
				{
				case 1:
					distanciaReferencia = 25;
					break;
				case 2:
					distanciaReferencia = 50;
					break;
				case 3:
					distanciaReferencia = 70;
					break;
				case 4:
					distanciaReferencia = 100;
					break;
				case 5:
					distanciaReferencia = 200;
					break;
				case 6:
					distanciaReferencia = 500;
					break;
				case 7:
					distanciaReferencia = 750;
					break;
				}
				printMenuCargaArchivo();
				int cargaArchivo = sc.nextInt();
				boolean usarPersistencia = false;
				switch (cargaArchivo)
				{
				case 1:
					usarPersistencia = true;
					break;
				case 2:
					break;
				}
				System.out.println("Datos a cargar: " + linkJson);
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.loadServices(linkJson, distanciaReferencia, usarPersistencia);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

					
			case 2: //Crear el JSON
				
				String msg=Controller.generarJSON();
				System.out.println(msg);

				break;

			case 3: //Exit

				fin=true;
				sc.close();
				break;
			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Cargar toda la informacion del sistema de una fuente de datos (small, medium o large).");
		System.out.println("2. Generar el archivo JSON con la información cargada.");
		System.out.println("3. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	private static void printMenuDistancia()
	{
		System.out.println("-- Elija la distancia de referencia que quiere utilizar para la creacion del grafo");
		System.out.println("-- 1. 25 metros");
		System.out.println("-- 2. 50 metros");
		System.out.println("-- 3. 70 metros");
		System.out.println("-- 4. 100 metros");
		System.out.println("-- 5. 250 metros");
		System.out.println("-- 6. 500 metros");
		System.out.println("-- 7. 750 metros");
		System.out.println("-- Ingrese el numero correspondiente a la distancia de referencia que desea cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
	
	private static void printMenuCargaArchivo() {
		System.out.println("-- ¿Desea cargar el grafo de un JSON creado previamente por esta aplicación utilizando persistencia?");
		System.out.println("-- 1. Sí");
		System.out.println("-- 2. No");
		System.out.println("-- Ingrese el numero correspondiente a su respuesta y presione <Enter> para confirmar: (e.g., 1)");
	}
	
	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
}

